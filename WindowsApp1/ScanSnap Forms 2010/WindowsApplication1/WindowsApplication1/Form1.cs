using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using InteropTest;
using System.Windows.Input;
using System.Threading;
using System.Threading.Tasks;


namespace WindowsApplication1
{
    public partial class Form1 : Form
    {

        private ScanLib _scan;
        private Task _scanTask;
        public Form1()
        {
            InitializeComponent();
            _scan = new ScanLib();
           
        }

       private void Form1_Load_1(object sender, EventArgs e)
        {

        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Console.WriteLine("Key Pressed");
            if (_scanTask != null && !_scanTask.IsCompleted)
            {
                return;
            }
            _scanTask = Task.Run(() =>
            {
                _scan.reserve();
                _scan.scan();
            }
            );



        }


    }
}