using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace InteropTest
{
    public class ScanLib
    {
        public static int WM_COPYDATA = 0x004a;

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct SS_NOTIFY
        {
            public System.UInt32 Mode;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
            public string AppName;
        }

        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public UInt32 cbData;
            public UInt32 lpData;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct SS_SCAN
        {
            public System.UInt32 Mode;
            public System.Byte ScanningSide;
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        public IntPtr hWnd = FindWindow("ScanSnap Manager MainWndClass", null);

        public void reserve()
        {

            // This section Sends Reserve Message
            SS_NOTIFY notify = new SS_NOTIFY();
            notify.Mode = 0;
            notify.AppName = "WindowsApplication1";

            int nSizeNotify = Marshal.SizeOf(notify);
            IntPtr pSS_NOTIFY = Marshal.AllocHGlobal(nSizeNotify);
            Marshal.StructureToPtr(notify, pSS_NOTIFY, true);

            COPYDATASTRUCT data = new COPYDATASTRUCT();
            data.dwData = (IntPtr)2;
            //data.dwData = (IntPtr)1;
            data.cbData = (uint)nSizeNotify;
            data.lpData = (uint)pSS_NOTIFY;
            int nSizeData = Marshal.SizeOf(data);
            IntPtr pDATA = Marshal.AllocHGlobal(nSizeData);
            Marshal.StructureToPtr(data, pDATA, true);
            int rc = SendMessage(hWnd, WM_COPYDATA, (IntPtr)null, pDATA);
        }


        public void scan()
        {
            // This section Sends StartScan Message
            SS_SCAN scan = new SS_SCAN();
            scan.Mode = 0;
            scan.ScanningSide = 0;
            int nSizeScan = Marshal.SizeOf(scan);
            IntPtr pSS_SCAN = Marshal.AllocHGlobal(nSizeScan);
            Marshal.StructureToPtr(scan, pSS_SCAN, true);

            COPYDATASTRUCT data2 = new COPYDATASTRUCT();
            data2.dwData = (IntPtr)3;
            data2.cbData = (uint)nSizeScan;
            data2.lpData = (uint)pSS_SCAN;
            int nSizeData = Marshal.SizeOf(data2);
            nSizeData = Marshal.SizeOf(data2);
            IntPtr pDATA2 = Marshal.AllocHGlobal(nSizeData);
            Marshal.StructureToPtr(data2, pDATA2, true);
            int rc = SendMessage(hWnd, WM_COPYDATA, (IntPtr)null, pDATA2);
        }

        public void release()
        {
            // This section Sends Release Message
            SS_NOTIFY notify2 = new SS_NOTIFY();
            notify2.Mode = 1;
            notify2.AppName = "WindowsApplication1";
            int nSizeNotify = Marshal.SizeOf(notify2);

            nSizeNotify = Marshal.SizeOf(notify2);
            IntPtr pSS_NOTIFY2 = Marshal.AllocHGlobal(nSizeNotify);
            Marshal.StructureToPtr(notify2, pSS_NOTIFY2, true);

            COPYDATASTRUCT data3 = new COPYDATASTRUCT();
            data3.dwData = (IntPtr)2;
            data3.cbData = (uint)nSizeNotify;
            data3.lpData = (uint)pSS_NOTIFY2;
            int nSizeData = Marshal.SizeOf(data3);
            nSizeData = Marshal.SizeOf(data3);
            IntPtr pDATA3 = Marshal.AllocHGlobal(nSizeData);
            Marshal.StructureToPtr(data3, pDATA3, true);
            int rc = SendMessage(hWnd, WM_COPYDATA, (IntPtr)null, pDATA3);
        }
    }
}
